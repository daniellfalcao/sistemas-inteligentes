import random
from datetime import datetime

import numpy as np

from algoritmos_geneticos.chromosome import Chromosome


class AG:
    population = 20
    chromosomes = Chromosome.generate_population(population, np.random.uniform(-0.5, 0.5, 26))
    max_generations = 500
    continue_loop = True
    fitness_count = 0
    mutation_type = 1

    taxa_crossover = 0.75
    taxa_mutation = 0.1

    def get_chromosome(self, chromosomes):

        roleta = np.array([])

        for chromosome in chromosomes:
            for i in range(int(chromosome.probability * 100)):
                roleta = np.hstack((roleta, np.array([chromosome])))

        return roleta[random.randint(0, roleta.shape[0] - 1)]

    def crossover_blx(self, father1, father2, a):

        if np.random.uniform(0, 1) >= AG.taxa_crossover:

            b = np.random.uniform(-a, 1 + a)
            genes_son1 = father1.genes + (b * (father2.genes - father1.genes))
            b = np.random.uniform(-a, 1 + a)
            genes_son2 = father2.genes + (b * (father2.genes - father1.genes))
            return Chromosome(genes_son1), Chromosome(genes_son2)
        else:
            return father1, father2

    def mutation_gaussian(self, genes):
        for index in range(genes.shape[0]):
            if np.random.uniform(0, 1) >= AG.taxa_mutation:
                genes[index] = genes[index] + np.random.normal(0, 0.1, 1)[0]
        return genes

    def calc_fitness(self, chromosomes):
        for chromosome in chromosomes:
            AG.fitness_count += 1
            chromosome.update_fitness()

        if AG.fitness_count >= 2000:
            AG.continue_loop = False

    def calc_probability(self, chromosomes):
        total_fitness = np.sum(chromosomes, dtype=float)
        for chromosome in chromosomes:
            chromosome.update_probability(total_fitness)

    def order_by_ranking(self, chromosomes):
        return np.sort(chromosomes)

    def execute(self, taxa_crossover, taxa_mutation):

        AG.taxa_mutation = taxa_mutation
        AG.taxa_crossover = taxa_crossover

        now = datetime.now()

        self.calc_fitness(AG.chromosomes)
        AG.chromosomes = self.order_by_ranking(AG.chromosomes)
        self.calc_probability(AG.chromosomes)

        geracao = 0
        while AG.continue_loop:

            children = np.array([])
            for i in range(int(AG.population / 2)):
                son1, son2 = self.crossover_blx(self.get_chromosome(AG.chromosomes), self.get_chromosome(AG.chromosomes), 0.5)
                children = np.hstack((children, np.array([son1])))
                children = np.hstack((children, np.array([son2])))

            # mutacao
            for son in children:
                son.genes = self.mutation_gaussian(son.genes)

            AG.chromosomes = np.insert(AG.chromosomes, 0, children)

            self.calc_fitness(AG.chromosomes)
            AG.chromosomes = self.order_by_ranking(AG.chromosomes)
            AG.chromosomes = AG.chromosomes[0:AG.population]
            self.calc_probability(AG.chromosomes)

            geracao += 1

        print("melhor cromosomo")
        print("tempo:" + str(datetime.now() - now))
        print("fitness:" + str(AG.chromosomes[0].fitness) + "\n")

        AG.chromosomes[0].test()

        # fig, ax = plt.subplots()
        #
        # for index in range(len(cities)):
        #     plt.scatter(cities[index].x, cities[index].y, marker='o')
        #
        # x = []
        # y = []
        #
        # for index in range(chromosomes[0].genes.shape[0]):
        #     x.append(chromosomes[0].genes[index].x)
        #     y.append(chromosomes[0].genes[index].y)
        #
        # plt.plot(x, y)
        # plt.show()
