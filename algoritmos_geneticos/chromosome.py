import numpy as np

from multi_layer_perceptron.mlp import MLP
from file_reader.filereader import FileReader


class Chromosome:
    entrys_train = FileReader.get_fuel_injection("x", "train")
    entry_test = FileReader.get_fuel_injection("x", "test")
    outs_train = FileReader.get_fuel_injection("d", "train")
    outs_test = FileReader.get_fuel_injection("d", "test")

    mlp = None

    def __init__(self, genes):
        self.genes = genes
        self.fitness = 0.0
        self.probability = 0

    def __lt__(self, other):
        return self.fitness > other.fitness

    def __float__(self):
        return self.fitness

    def update_fitness(self):

        Chromosome.mlp = MLP()

        Chromosome.mlp.setup(
            entrys=Chromosome.entrys_train,
            outs=Chromosome.outs_train,
            learning_rate=0,
            precision=0,
            momentum=0,
            neurons_in_hidden_layer=5,
            normalize=False,
            training_type=Chromosome.mlp.TRAINING_ONLINE,
            weights=self.genes
        )
        self.fitness = Chromosome.mlp.train()[0]

    def update_probability(self, total):
        self.probability = round(self.fitness / total, 2)

    def test(self):
        Chromosome.mlp.test(Chromosome.entry_test, Chromosome.outs_test)

    @staticmethod
    def generate(genes, permutate):
        if permutate:
            return Chromosome(np.take(genes, np.random.permutation(genes.shape[0]), axis=0))
        else:
            return Chromosome(genes)

    @staticmethod
    def generate_population(qtd, genes):
        population = np.empty([qtd], dtype=Chromosome)
        for x in range(qtd):
            population[x] = Chromosome.generate(genes, True)
        return population
