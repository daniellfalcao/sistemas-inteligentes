from functions import functions as f


class City:

    def __init__(self, coordinate_x, coordinate_y):
        self.x = int(coordinate_x)
        self.y = int(coordinate_y)

    def distance_to(self, city):
        return f.calc_dist_two_points(self.x, self.y, city.x, city.y)

    @staticmethod
    def distance_total(cities):
        distance = 0
        for x in range(len(cities)):
            if x != (len(cities) - 1):
                distance += cities[x].distance_to(cities[x + 1])
            else:
                distance += cities[x].distance_to(cities[0])
        return distance
