import os
import re

import numpy as np

from algoritmos_geneticos.city import City


class FileReader:

    @staticmethod
    def convert_data(row):
        return re.sub('\t', ' ', row).strip().split(" ")

    @staticmethod
    def get_cities(qtd):
        file = open(os.path.normpath(os.getcwd() + os.sep + os.pardir + "\\sistemas-inteligentes\\file_reader\\files\\" + "ncit30.dat"), "r")
        lines = file.readlines()
        file.close()

        cities = np.empty([qtd], City)

        for i in range(cities.shape[0]):
            coordinate = FileReader.convert_data(lines[i])
            cities[i] = City(coordinate[0], coordinate[1])

        return cities

    @staticmethod
    def get_fuel_injection(x_or_d, test_or_train):
        file = open(os.path.normpath(os.getcwd() + os.sep + os.pardir + "\\sistemas-inteligentes-Daniel\\file_reader\\files\\" + str(x_or_d) + str(test_or_train) + "_injection.txt"), "r")
        dataset_list = file.readlines()
        file.close()

        matrix = [[]]

        for line in dataset_list:
            try:
                matrix = np.vstack((matrix, [FileReader.convert_data(line)]))
            except:
                matrix = np.array([FileReader.convert_data(dataset_list[0])])

        return matrix.astype(float)
