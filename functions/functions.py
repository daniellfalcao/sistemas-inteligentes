import numpy as np


def calc_dist_gaussian(x, o, u):
    return (1 / (o * np.sqrt(2 * 3.14))) * (np.exp(-1 / 2 * ((x - u) / o) ** 2))


def step_function(value):
    if value >= 0:
        return 1
    else:
        return -1


def dx_tangh(value):
    return 1 - np.power(np.tanh(value), 2)


def calc_dist_two_points(x1, y1, x2, y2):
    if not (x1 == x2 and y1 == y2):
        if x1 - x2 == 0 or y1 - y2 == 0:
            if x1 - x2 == 0:
                return abs(y1 - y2)
            elif y1 - y2 == 0:
                return abs(x1 - x2)
            else:
                return 0.0
        else:
            return np.hypot(abs(x1 - x2), abs(y1 - y2))
    else:
        return 0.0
