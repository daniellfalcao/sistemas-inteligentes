import numpy
import random
from file_reader.filereader import FileReader
from multi_layer_perceptron.mlp import MLP


class particula:
    entrys_train = FileReader.get_fuel_injection("x", "train")
    entry_test = FileReader.get_fuel_injection("x", "test")
    outs_train = FileReader.get_fuel_injection("d", "train")
    outs_test = FileReader.get_fuel_injection("d", "test")

    mlp = None

    def update_fitness(self):
        particula.mlp = MLP()

        particula.mlp.setup(
            entrys=particula.entrys_train,
            outs=particula.outs_train,
            learning_rate=0,
            precision=0,
            momentum=0,
            neurons_in_hidden_layer=5,
            normalize=False,
            training_type=particula.mlp.TRAINING_ONLINE,
            weights=numpy.asarray(self.posicao)
        )
        self.fitness = particula.mlp.train()[0]

    def __init__(self):
        self.posicao = []
        self.particulaMinimoI = -1
        self.velocidade = []
        self.vizinhos = []
        self.espacoBusca = [-0.5, 0.5]

        for i in range(0, 26):
            self.posicao.append(random.uniform(-0.5, 0.5))
            self.velocidade.append(random.uniform(-0.5, 0.5))

        self.update_fitness()

    def atualizarVelocidade(self, c1, c2, posG):
        if (posG != -1):
            for i in range(0, len(self.velocidade)):
                r1 = random.random()
                r2 = random.random()
                self.velocidade[i] = c1 * r1 * (self.particulaMinimoI.posicao[i] - self.posicao[i]) + \
                                     c2 * r2 * (posG[i] - self.posicao[i]) + \
                                     self.velocidade[i]
        else:
            for i in range(0, len(self.velocidade)):
                r1 = random.random()
                r2 = random.random()
                self.velocidade[i] = c1 * r1 * (self.particulaMinimoI[i] - self.posicao[i]) + \
                                     c2 * r2 * (self.particulaMinimoI[i] - self.posicao[i]) + \
                                     self.velocidade[i]

    def atualizarvelocidadeInercia(self, w, c1, c2, melhorPosGlob):
        for i in range(0, len(self.velocidade)):
            r1 = random.random()
            r2 = random.random()
            self.velocidade[i] = c1 * r1 * (self.particulaMinimoI[i] - self.particulaMinimoI[i]) + \
                                 c2 * r2 * (melhorPosGlob[i] - self.posicao[i]) + \
                                 w * self.velocidade[i]

    def atualizarvelocidadeConstricao(self, coefConstricao, c1, c2, melhorPosGlob):
        x = coefConstricao
        for i in range(0, len(self.velocidade)):
            r1 = random.random()
            r2 = random.random()
            self.velocidade[i] = x * (c1 * r1 * (self.particulaMinimoI[i] - self.posicao[i]) + \
                                      c2 * r2 * (melhorPosGlob[i] - self.posicao[i]) + \
                                      self.velocidade[i])

    def atualizarPosicao(self, espacoBusca):
        for i in range(0, len(self.posicao)):
            self.posicao[i] = self.posicao[i] - self.velocidade[i]
            # ajustar posicao maxima se necessário
            if self.posicao[i] > self.espacoBusca[1]:
                self.posicao[i] = self.espacoBusca[1]
            # ajustar posicao minima se necessário
            if self.posicao[i] < self.espacoBusca[0]:
                self.posicao[i] = self.espacoBusca[0]

    def test(self):
        particula.mlp.test(particula.entry_test, particula.outs_test)
