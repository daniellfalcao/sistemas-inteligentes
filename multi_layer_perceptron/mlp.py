import matplotlib.pyplot as plt
import numpy as np

import functions.functions as f


class MLP:
    TRAINING_ONLINE = 1
    TRAINING_OFFLINE = 2

    def __init__(self):
        self.epochs = 0
        self.learning_rate = 1
        self.precision = 0.000001
        self.momentum = 0
        self.eqm_current = 0
        self.eqm_previous = 10
        self.entry = np.empty([0])
        self.out = np.empty([0])
        self.old_weight_hidden_layer = np.empty([0])
        self.old_weight_output = np.empty([0])
        self.current_weight_hidden_layer = np.empty([0])
        self.current_weight_output = np.empty([0])
        self.new_weight_hidden_layer = np.empty([0])
        self.new_weight_output = np.empty([0])
        self.activation_potential_hidden_layer = np.empty([0])
        self.activation_potential_output = np.empty([0])
        self.neuron_out_hidden_layer = np.empty([0])
        self.neuron_out_output = np.empty([0])
        self.gradient_output = np.empty([0])
        self.gradient_hidden_layer = np.empty([0])
        self.neurons_in_input_layer = 0
        self.neurons_in_hidden_layer = 0
        self.neurons_in_output_layer = 0
        self.epochs_array = np.empty([0])
        self.eqm_array = np.empty([0])

    def setup(self, entrys, outs, learning_rate, precision, momentum, neurons_in_hidden_layer, normalize,
              training_type, weights):

        self.learning_rate = learning_rate
        self.momentum = momentum
        self.precision = precision
        self.neurons_in_input_layer = entrys.shape[1]
        self.neurons_in_hidden_layer = neurons_in_hidden_layer
        self.neurons_in_output_layer = 1

        # insere -1 na primeira coluna de todas as linhas da entrada
        entrys = np.insert(entrys, 0, [-1 for column in entrys], axis=1)
        self.entry = entrys

        self.out = outs

        if normalize:
            vec = np.sqrt(np.sum(np.power(self.entry, 2)))
            for line in range(self.entry.shape[0]):
                for column in range(self.entry.shape[1]):
                    self.entry[line][column] = self.entry[line][column] / vec

        # randomiza a seed
        np.random.seed(np.random.random_integers(1, 1000, 1)[0])

        # seta os pesos iniciais da layer escondida
        for index in range(neurons_in_hidden_layer):
            weight = weights[(index * 4):(index * 4) + 4]
            try:
                self.new_weight_hidden_layer = np.vstack((self.new_weight_hidden_layer, [weight]))
            except:
                self.new_weight_hidden_layer = np.array([weight])

            # seta os pesos iniciais da saida
            weight = weights[weights.shape[0] - 6:weights.shape[0] + 1]
            self.new_weight_output = np.array([weight])

        self.old_weight_hidden_layer = self.new_weight_hidden_layer.copy()
        self.old_weight_output = self.new_weight_output.copy()
        self.current_weight_hidden_layer = self.new_weight_hidden_layer.copy()
        self.current_weight_output = self.new_weight_output.copy()

    def forward(self, row):

        # calculo do potencial de ativacao da entrada da primeira e unica camada escondida
        # Ij(1) = (nΣi=0)Wij*Xi
        self.activation_potential_hidden_layer = self.new_weight_hidden_layer.dot(self.entry[row])

        # calculo da saida do neuronio da primeira e unica camada escondida
        # Yj(1) = tangh(Ij(1))
        self.neuron_out_hidden_layer = np.vstack((np.array([-1]),
                                                  np.tanh([self.activation_potential_hidden_layer]).T))

        # calculo do potencial de ativacao do output da rede
        # Ij(2) = (n(L-1)Σi=0)Wij*Yi(L-1)

        self.activation_potential_output = self.new_weight_output.dot(self.neuron_out_hidden_layer)

        # calculo da saida do neuronio de resultado
        # Yj(2) = tangh(Ij(2))
        self.neuron_out_output = self.activation_potential_output
        # print(self.neuron_out_output[0])

    def backward(self, row):

        # reseta as variaveis de gradiente
        self.gradient_output = np.empty([0])
        self.gradient_hidden_layer = np.empty([0])

        # calcula o gradiente da saida
        for neuron in range(self.neurons_in_output_layer):
            gradient = (self.out[row][neuron] - self.neuron_out_output[neuron]) * f.dx_tangh(
                self.activation_potential_output[neuron])
            try:
                self.gradient_output = np.vstack((self.gradient_output, [gradient]))
            except:
                self.gradient_output = np.array([gradient])

        # ajuste de pesos da saida
        for line in range(self.new_weight_output.shape[0]):
            for column in range(self.new_weight_output.shape[1]):
                self.new_weight_output[line][column] += \
                    self.momentum * (self.current_weight_output[line][column] - self.old_weight_output[line][column]) \
                    + self.learning_rate * self.gradient_output[line] * self.neuron_out_hidden_layer[column]

        # calcular o gradiente da camada escondida
        for neuron in range(self.neurons_in_hidden_layer):
            sum = 0
            for out_neuron in range(self.neurons_in_output_layer):
                sum += self.gradient_output[out_neuron] * \
                       self.new_weight_output[out_neuron][neuron + 1]  # neuron + 1 para compensar o peso do bias
            gradient = sum * f.dx_tangh(self.activation_potential_hidden_layer[neuron])
            try:
                self.gradient_hidden_layer = np.vstack((self.gradient_hidden_layer, [gradient]))
            except:
                self.gradient_hidden_layer = np.array([gradient])

        # ajuste de pesos da camada escondida
        for line in range(self.new_weight_hidden_layer.shape[0]):
            for column in range(self.new_weight_hidden_layer.shape[1]):
                self.new_weight_hidden_layer[line][column] += \
                    self.momentum * (
                            self.current_weight_hidden_layer[line][column] - self.old_weight_hidden_layer[line][
                        column]) + \
                    self.learning_rate * self.gradient_hidden_layer[line] * self.entry[row][column]

        self.old_weight_hidden_layer = self.current_weight_hidden_layer.copy()
        self.old_weight_output = self.current_weight_output.copy()
        self.current_weight_hidden_layer = self.new_weight_hidden_layer.copy()
        self.current_weight_output = self.new_weight_output.copy()

    def calc_eqm(self):
        eqm = 0
        for k in range(self.entry.shape[0]):
            eqm += self.calc_error(k)
        return eqm / self.entry.shape[0]

    def calc_error(self, k):
        sum = 0
        for i in range(self.neurons_in_output_layer):
            sum += (self.out[k][i] - self.neuron_out_output[i]) ** 2
        return sum / 2

    def plot_eqm_epoch(self):
        plt.tight_layout()
        plt.plot(self.epochs_array, self.eqm_array, '-r', color='k')
        plt.title('EQM x ÉPOCA')
        plt.xlabel('ÉPOCA', color='#000000')
        plt.ylabel('EQM', color='#000000')
        plt.grid()
        plt.show()

    def train(self):

        for row in range(self.entry.shape[0]):
            self.forward(row)

        return 1 / (1 + self.calc_eqm())

    def test(self, entrys, outs):

        # insere -1 na primeira coluna de todas as linhas da entrada
        entrys = np.insert(entrys, 0, [-1 for column in entrys], axis=1)
        self.entry = entrys
        self.out = outs

        total_hits = 0
        total_mistakes = 0

        for line in range(self.out.shape[0]):
            hits = 0
            mistakes = 0
            self.forward(line)
            for column in range(self.neurons_in_output_layer):
                if round(self.out[line][column], 1) == round(self.neuron_out_output[column][0], 1):
                    hits += 1
                    print("*VALOR ESPERADO = " + str(self.out[line][column]) + " ==> RESULTADO = " + str(self.neuron_out_output[column][0]))
                else:
                    mistakes += 1
                    print("VALOR ESPERADO = " + str(self.out[line][column]) + " ==> RESULTADO = " + str(self.neuron_out_output[column][0]))

            total_hits += hits
            total_mistakes += mistakes
            # print("amostra = " + str(line) + " acertos = " + str(hits) + " erros = " + str(mistakes))

        print("porcentagem de acerto = " + str((100 * total_hits) / (total_mistakes + total_hits)))
