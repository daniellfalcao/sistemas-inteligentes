from otimizacao_por_enxame_de_particulas.particula import particula
from file_reader.filereader import FileReader
from multi_layer_perceptron.mlp import MLP

class PSO:
    def __init__(self, tamanhoPopulacao, topologia,
                 pesoInercia, constricao, c1, c2):
        self.populacao = []
        self.particulaMinimoG = -1
        self.w = pesoInercia
        self.x = constricao
        self.c1 = c1
        self.c2 = c2
        self.contadorFitness = 0
        self.topologia = topologia
        self.mlp = MLP()
        #--INICIALIZANDO POPULACAO--
        for i in range(tamanhoPopulacao):
            self.populacao.append(particula())

    def avaliarTopologiaAnel(self, pos):
        if(pos == 0):
            particulaAtual = self.populacao[pos]
            particulaDireita = self.populacao[pos+1]
            maior = max(particulaAtual.fitness, particulaDireita.fitness)
            if(maior == particulaAtual.fitness):
                self.populacao[pos].particulaMinimoI = particulaAtual
            else:
                self.populacao[pos].particulaMinimoI = particulaDireita
        elif(pos < len(self.populacao) - 1):
            particulaAtual = self.populacao[pos]
            particulaDireita = self.populacao[pos + 1]
            particulaEsquerda = self.populacao[pos - 1]
            #--PEGA O MAIOR DOS 3--
            maior = max(particulaAtual.fitness, max(particulaDireita.fitness, particulaEsquerda.fitness))
            #--DEFINI O MÍNIMO LOCAL DE ACORDO COM O ATUAL E OS VIZINHOS--
            if(maior == particulaAtual.fitness):
                self.populacao[pos].particulaMinimoI = particulaAtual
            elif(maior == particulaDireita.fitness):
                self.populacao[pos].particulaMinimoI = particulaDireita
            else:
                self.populacao[pos].particulaMinimoI = particulaEsquerda

    def avaliarTopologiaEstrela(self):
        if(self.particulaMinimoG == -1):
            atual = self.populacao[0]
            pos = 0
            for i in range(1,len(self.populacao)):
                if(atual.fitness < self.populacao[i].fitness):
                    atual = self.populacao[i]
            self.particulaMinimoG = atual
        else:
            atual = self.particulaMinimoG
            for i in range(0,len(self.populacao)):
                if(atual.fitness < self.populacao[i].fitness):
                    atual = self.populacao[i]
            self.particulaMinimoG = atual

    def avaliar(self):
        for i in range(len(self.populacao)):
            if(self.populacao[i].particulaMinimoI == -1):
                self.populacao[i].particulaMinimoI = self.populacao[i]
            else:
                if(self.populacao[i].particulaMinimoI.fitness < self.populacao[i].fitness):
                    self.populacao[i].particulaMinimoI = self.populacao[i]

    def atualizarContadorFitness(self):
        for i in range(len(self.populacao)):
            self.contadorFitness += self.populacao[i].fitness
        # print("Contador fitness: " + str(self.contadorFitness) + "\nMinimo global: " + str(self.particulaMinimoG))
        # print("-------------------------------------------------------------------------------------------")

    def executar(self):

        while(self.contadorFitness < 2000):
            #--ATUALIZA CONTADOR--
            self.atualizarContadorFitness()
            #--BUSCA OS MINIMOS GLOBAL E LOCAL--
            self.avaliar()
            if (self.topologia == "ESTRELA"):
                self.avaliarTopologiaEstrela()
            #--ATUALIZA VELOCIDADE--
            for i in range(len(self.populacao)):
                if (self.topologia == "ANEL"):
                    self.avaliarTopologiaAnel(i)
                if(self.particulaMinimoG == -1):
                    if (self.w != -1):
                        self.populacao[i].atualizarvelocidadeInercia(self.w, self.c1, self.c2, self.particulaMinimoG.posicao)
                    elif (self.x != -1):
                        self.populacao[i].atualizarvelocidadeConstricao(self.x, self.c1, self.c2, self.particulaMinimoG.posicao)
                    else:
                        self.populacao[i].atualizarVelocidade(self.c1, self.c2, self.populacao[i].particulaMinimoI.posicao)
                else:
                    if (self.w != -1):
                        self.populacao[i].atualizarvelocidadeInercia(self.w, self.c1, self.c2, self.populacao[i].particulaMinimoI.posicao)
                    elif (self.x != -1):
                        self.populacao[i].atualizarvelocidadeConstricao(self.x, self.c1, self.c2, self.populacao[i].particulaMinimoI.posicao)
                    else:
                        self.populacao[i].atualizarVelocidade(self.c1, self.c2, self.populacao[i].particulaMinimoI.posicao)






if __name__ == "__main__":
    execucao1 = PSO(20, "ESTRELA", -1, -1, 2.05, 2.05)
    execucao1.executar()
    execucao2 = PSO(20, "ANEL", -1, -1, 2.05, 2.05)
    execucao2.executar()
    execucao3 = PSO(20, "", -1, -1, 2.05, 2.05)
    execucao3.executar()

    execucao1.particulaMinimoG.test()








